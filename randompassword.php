<?php

declare(strict_types=1);

/**
 * Generate a random password
 *
 * PHP version 8.0.0 or later
 *
 * @author    Walter Ebert
 * @link      http://walterebert.com Author
 * @copyright Copyright (c) 2008-2023 Walter Ebert
 * @license   https://spdx.org/licenses/MIT.html MIT
 * @version   3.0.0
 * @link      https://bitbucket.org/walterebert/random-password-generator Repository
 */

/**
 * Generate random integers
 *
 * @param  integer $max Maximum number
 *
 * @return integer
 */
function wee_random_int(int $max): int
{
    if (class_exists('Random')) {
        $r = new Random\Randomizer();
        return $r->getInt(0, $max);
    }

    return random_int(0, $max);
}

/**
 * Get string length
 *
 * @param  string  $string Input string
 *
 * @return integer
 */
function wee_strlen(string $string): int
{
    if (function_exists('mb_strlen')) {
        return mb_strlen($string, 'UTF-8');
    }

    return strlen($string);
}

/**
 * Return part of a string
 *
 * @param  string  $string Input string
 * @param  integer $start  Start position
 * @param  integer $lenght Length of returned string
 *
 * @return string
 */
function wee_substr(string $string, int $start, int $length): string
{
    if (function_exists('mb_substr')) {
        return mb_substr($string, $start, $length, 'UTF-8');
    }

    return substr($string, $start, $length);
}

/**
 * Randomly shuffle a string
 *
 * @param  string $string Input string
 *
 * @return string
 */
function wee_str_shuffle(string $string): string
{
    $length = wee_strlen($string);
    $remaining = $string;
    $shuffled = '';

    for ($i = 0; $i < $length; $i++) {
        $lengthLeft = wee_strlen($remaining);

        if ($lengthLeft > 0) {
            $position = wee_random_int($lengthLeft - 1);
            $random = wee_substr(
                $remaining,
                $position,
                1
            );

            $remaining = wee_substr($remaining, 0, $position) . wee_substr($remaining, $position + 1, $lengthLeft - 1);
        }

        $shuffled .= $random;
    }

    return $shuffled;
}

/**
 * Generate a random password
 *
 * @param  integer $length   password length (default: 8)
 * @param  string  $type     password type (default: alphanumeric)
 * @param  mixed   $custom   custom set of characters (optional)
 *
 * @return string  generated password
 */
function wee_randomPassword(int $length = 8, string $type = '', string|array $custom = ''): string
{
    // Select password type
    if (function_exists('mb_strtolower')) {
        $type = mb_strtolower($type, 'UTF-8');
    } else {
        $type = strtolower($type);
    }

    switch ($type) {
        case 'alphabetical':
            $chars = [
                'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                'abcdefghijklmnopqrstuvwxyz'
            ];
            break;

        case 'ascii':
            $chars = [
                'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                'abcdefghijklmnopqrstuvwxyz',
                '0123456789',
                '`~!@#$%^&*()-_=+\\|[{]};:\'",<.>/?'
            ];
            break;

        case 'hexadecimal':
            $chars = [
                '0123456789',
                'abcdef'
            ];
            break;

        case 'latin-1':
        case 'latin1':
            $upper = '';
            for ($i = 192; $i < 215; $i++) {
                $upper .= chr($i);
            }
            for ($i = 216; $i < 222; $i++) {
                $upper .= chr($i);
            }
            $lower = '';
            for ($i = 224; $i < 247; $i++) {
                $lower .= chr($i);
            }
            for ($i = 248; $i < 256; $i++) {
                $lower .= chr($i);
            }
            if (function_exists('mb_convert_encoding')) {
                $upper = mb_convert_encoding($upper, 'UTF-8', 'ISO-8859-1');
                $lower = mb_convert_encoding($lower, 'UTF-8', 'ISO-8859-1');
            } else {
                $upper = utf8_encode($upper);
                $lower = utf8_encode($lower);
            }
            $chars = [
                'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                'abcdefghijklmnopqrstuvwxyz',
                '0123456789',
                '`~!@#$%^&*()-_=+\\|[{]};:\'",<.>/?',
                $upper,
                $lower
            ];
            break;

        case 'numeric':
            $chars = ['0123456789'];
            break;

        case 'reduced':
            $chars = [
                'ABCDEFGHJKLMNPQRSTUVWXYZ',
                'abcdefhijkmnpqrstuvwxyz',
                '2345678'
            ];
            break;

        case 'custom':
            $chars = (array) $custom ;
            break;

        case 'alphanumeric':
        default:
            $chars = [
                'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                'abcdefghijklmnopqrstuvwxyz',
                '0123456789'
            ];
    }

    // Set chunk sizes
    $keys = $chunks = [];
    $numchars = count($chars);
    $chunk = ceil($length / $numchars);
    foreach ($chars as $key => $char) {
        $keys[$key] = $key;
        $chunks[$key] = (int) $chunk;
    }
    $chunksum = (int) $chunk * $numchars;
    if ($chunksum > $length) {
        $cuts = (array) array_rand($keys, $chunksum - $length);
        foreach ($chunks as $key => $chunk) {
            if (in_array($key, $cuts)) {
                $chunks[$key]--;
            }
        }
    }

    // Create password parts
    $parts = [];
    foreach ($chars as $key => $value) {
        $parts[$key] = '';
        do {
            $parts[$key] .= wee_substr(wee_str_shuffle($value), 0, $chunks[$key]);
            $count = wee_strlen($parts[$key]);
        } while ($count < $chunks[$key]);

        if ($count > $chunks[$key]) {
            $parts[$key] = wee_substr($parts[$key], 0, $chunks[$key]);
        }
    }

    // Create password string
    $password = wee_str_shuffle(implode('', $parts));

    return $password;
}
