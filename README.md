# Random Password Documentation

Requires PHP 8.0 or later

## Description
```
string wee_randomPassword( [ int length [, string type [, mixed charsets ]]] )
```

The function was written to create strong passwords with a decent level of control
over the final output. You can specify length and type of the password. It is also
possible to create a custom password type.

To achieve control over the output character sets are used. From each set an
equal amount of characters is selected. Also each character in a set has to be used, before
it can be selected again. This ensures that all character types are used and repeating characters are kept to a minimum.

The function returns a UTF-8 encoded string. Characters for a custom type must be UTF-8 encoded.

*Table: function parameters*

|parameter|description|
|:--------|:----------|
|length|password length (default: 8)|
|type|password type (default: alphanumeric)|
|charsets|custom character sets (only for type "custom")|

*Table: password types*

|type|character sets|
|:---|:-------------|
|alphanumeric *(default)*|`ABCDEFGHIJKLMNOPQRSTUVWXYZ`  `abcdefghijklmnopqrstuvwxyz`  `0123456789`|
|reduced *(=alphanumeric without 0oO9g1lI)*|`ABCDEFGHJKLMNPQRSTUVWXYZ`  `abcdefghijkmnpqrstuvwxyz`  `2345678`|
|alphabetical|`ABCDEFGHIJKLMNOPQRSTUVWXYZ`  `abcdefghijklmnopqrstuvwxyz`|
|ascii|`ABCDEFGHIJKLMNOPQRSTUVWXYZ`  `abcdefghijklmnopqrstuvwxyz`  `0123456789`  ``~`!@#$%^&*()-_=+\⎮[]{};:'",.<>/?``|
|latin1|`ABCDEFGHIJKLMNOPQRSTUVWXYZ`  `abcdefghijklmnopqrstuvwxyz`  `ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ`  `àáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ` `0123456789`  ``~`!@#$%^&*()-_=+\⎮[]{};:'",.<>/?``|
|hexadecimal|`0123456789`  `abcdef`|
|numeric|`0123456789`|
|custom|user defined *(UTF-8 encoded)*|

Although this function should create a strong password, there is no 100%
guarantee. There is still a small chance that this function creates a password
like: ABCabc123. To test the strength of a generated password, you can use for example
[cracklib](http://www.php.net/manual/en/ref.crack.php).

## Examples

```
require_once 'randompassword.php';

// Default behaviour (no parameters), e.g. 3pHJXd4l
$password1 = wee_randomPassword();

// Password with length 8 and type ascii, e.g. 7w$6aDR~
$password2 = wee_randomPassword(8, 'ascii');

// Password with length 6 and only lowercase letters, e.g. fkslhu
$password4 = wee_randomPassword(6, 'custom', 'abcdefghijklmnopqrstuvwxyz');

// Password with length 16 and only uppercase letters + numbers, e.g. 71H5N92GJK0I6E3A
$password5 = wee_randomPassword(16, 'custom', ['ABCDEFGHIJKLMNOPQRSTUVWXYZ', '0123456789']);
```

## License

The package is released under the [MIT License](https://spdx.org/licenses/MIT.html)
