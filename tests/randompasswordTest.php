<?php
/**
 * @requires PHP 8.2
 */

use PHPUnit\Framework\TestCase;

class RandompasswordTest extends TestCase
{
    public function testRandomness()
    {
        $password = wee_randomPassword(128, 'alphabetical');

        $countChars = count_chars($password);

        $max = 0;
        foreach ($countChars as $character => $count) {
            if ($count > $max) {
                $max = $count;
            }
        }

        $this->assertTrue($max < 4);
    }

    /**
     * @dataProvider stringProvider
     */
    public function testStrlen($string, $expected)
    {
        $this->assertTrue($expected === wee_strlen($string));
    }

    /**
     * @dataProvider utf8StringProvider
     */
    public function testUtf8Strlen($string, $expected)
    {
        $this->assertTrue($expected === wee_strlen($string));
    }

    /**
     * @dataProvider stringProvider
     */
    public function testShuffleLength($string, $expected)
    {
        $shuffled  = wee_str_shuffle($string);

        $this->assertTrue($expected === strlen($shuffled));
    }

    /**
     * @dataProvider utf8StringProvider
     */
    public function testUtf8ShuffleLength($string, $expected)
    {
        $shuffled  = wee_str_shuffle($string);

        $this->assertTrue($expected === wee_strlen($shuffled));
    }

    /**
     * @dataProvider typeProvider
     */
    public function testRandomPassword($length, $type)
    {
        $this->assertTrue($length === wee_strlen(wee_randomPassword($length, $type)));
    }

    /**
     * @dataProvider customTypeProvider
     */
    public function testCustomRandomPassword($length, $set)
    {
        $this->assertTrue($length === wee_strlen(wee_randomPassword($length, 'custom', $set)));
    }

    public static function stringProvider()
    {
        return [
            ['abc', 3],
            ['abcABC', 6],
            ['abcABC123', 9],
            ['abcABC123abcABC123', 18],
            ['abcABC123abcABC123abcABC123', 27],
            ['abcABC123abcABC123abcABC123abcABC123', 36],
            ['abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123', 72],
            ['abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123abcABC123', 144],
        ];
    }

    public static function utf8StringProvider()
    {
        return [
            ['äü😉', 3],
            ['äü😉ÄÜ🤔', 6],
            ['äü😉ÄÜ🤔123', 9],
            ['äü😉ÄÜ🤔123äü😉ÄÜ🤔123', 18],
            ['äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123', 27],
            ['äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123', 36],
            ['äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123', 72],
            ['äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123äü😉ÄÜ🤔123', 144],
        ];
    }

    public static function typeProvider()
    {
        $types = [
            'alphabetical',
            'ascii',
            'hexadecimal',
            'latin-1',
            'latin1',
            'numeric',
            'reduced',
        ];

        $lengths = [3, 6, 9, 18, 27, 36, 72, 144];

        $return = [];
        foreach ($types as $type) {
            foreach ($lengths as $length) {
                $return[] = [$length, $type];
            }
        }

        return $return;
    }

    public static function customTypeProvider()
    {
        $sets = [
            [
                'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                'abcdefghijklmnopqrstuvwxyz',
                'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ',
                'àáâãäåæçèéêëìíîïðñòóôõöøùúûüýþÿ',
                '0123456789',
                '"\'`~!@#$%^&*€'
            ],
            [
                'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                'abcdefghijklmnopqrstuvwxyz',
                '0123456789',
                'äüßö😉ÄÜÖ€🤔'
            ],
        ];

        $lengths = [3, 6, 9, 18, 27, 36, 72, 144];

        $return = [];
        foreach ($sets as $set) {
            foreach ($lengths as $length) {
                $return[] = [$length, $set];
            }
        }

        return $return;
    }
}