<?php
/*
 * Demonstration on how a random password can be generated.
 *
 * copyright (c) 2008-2019 Walter Ebert (http://www.walterebert.com)
 */

// Allowed types
$types = ['alphanumeric', 'reduced', 'alphabetical', 'numeric', 'ascii', 'latin1', 'hexadecimal'];

// Default settings
$type = 'reduced';
$length = 12;
$selected_length = $selected_type = [];

// Handle form data
if (isset($_POST['length']) and isset($_POST['type'])) {
    // Clean form data
    $_POST['type'] = preg_replace('/[^a-z1]/', '', $_POST['type']);
    $_POST['length'] = (int) $_POST['length'];

    // Determine settings
    if (in_array($_POST['type'], $types)) {
        $type = $_POST['type'];
    }
    if ($_POST['length'] > 5 and $_POST['length'] < 129) {
        $length = $_POST['length'];
    }
}

$selected_type[$type] = $selected_length[$length] = 'selected';

// Load function and generate password
require 'randompassword.php';
$password = wee_randomPassword($length, $type);

// Send MIME type and character encoding
header('Content-type: text/html; charset: utf-8;');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Random password generator</title>
<style>
body, p, td, input, select, option, textarea, button {font-family: bitstream vera sans, geneva, verdana, helvetica, arial, sans-serif;}
code {font-family: bitstream vera sans mono, monaco, andale mono, courier, courier new, monospace;}
#password {padding: 0 .3em; border: 1px solid #000; background-color: #ddd;}
</style>
</head>
<body>

<h1>Random password generator</h1>

<p>Random password:<br>
<code id="password"><?= htmlspecialchars($password, ENT_QUOTES, 'utf-8') ?></code></p>


<form action="<?= htmlspecialchars($_SERVER['PHP_SELF'], ENT_QUOTES) ?>" method="post">
<table>
<tr>
  <td><label for="length">length</label></td>
  <td>
    <select id="length" name="length">
<?php
for ($i = 6; $i < 129; $i++) {
    echo "\t<option value=\"" . $i . "\" " . (isset($selected_length[$i]) ? $selected_length[$i] : '') . ">" . $i . "</option>\n";
}
?>
    </select>
  </td>
</tr>
<tr>
  <td><label for="type">type</label></td>
  <td>
    <select id="type" name="type">
      <option value="alphanumeric" <?= (!empty($selected_type['alphanumeric'])) ? 'selected' : '' ?>>letters + numbers</option>
      <option value="reduced" <?= (!empty($selected_type['reduced'])) ? 'selected' : '' ?>>letters + numbers - 0oO9g1lI</option>
      <option value="ascii" <?= (!empty($selected_type['ascii'])) ? 'selected' : '' ?>>lettters + numbers + symbols</option>
      <option value="latin1" <?= (!empty($selected_type['latin1'])) ? 'selected' : '' ?>>latin-1</option>
      <option value="hexadecimal" <?= (!empty($selected_type['hexadecimal'])) ? 'selected' : '' ?>>hexadecimal</option>
      <option value="alphabetical" <?= (!empty($selected_type['alphabetical'])) ? 'selected' : '' ?>>letters</option>
      <option value="numeric" <?= (!empty($selected_type['numeric'])) ? 'selected' : '' ?>>numbers</option>
    </select>
  </td>
</tr>
<tr>
  <td></td>
  <td><input type="submit" value="Generate random password" /></td>
</tr>
</table>
</form>
